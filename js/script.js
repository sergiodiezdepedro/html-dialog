const modal = document.querySelector(".dialog");
const showButton = document.querySelector(".btn-show");
const hideButton = document.querySelector(".btn-hide");

showButton.addEventListener("click", () => {
    modal.showModal();
});

hideButton.addEventListener("click", () => {
    modal.close();
});
